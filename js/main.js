var puntajeHtml = document.getElementById('puntaje');
var disparosHtml = document.getElementById('disparos');
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var disparo = new Audio('./sound/disparo.mp3');
var campana = new Audio('./sound/campana.mp3');
var mira = new Image(50);
mira.src = './img/mira.svg';
var mouseX = 0;
var mouseY = 0;
// ctx.fillRect(0,0,400,400)
var puntos = 0;
var disparos = 30;
var posicionX = 0;
var posicionY = 0;
var movimiento_x = 15;
var movimiento_y = 10;
var blanco = new Image(80);
blanco.src = './img/blanco.svg';
var ancho_juego = 600;
var alto_juego = 400;
canvas.width = ancho_juego;
canvas.height = alto_juego;
function sonidoDisparo() {
    disparo.currentTime = 0;
    disparo.play();
}
function sonidoCampana() {
    campana.currentTime = 0;
    campana.play();
}
function acerto(e) {
    if (e.offsetX >= (posicionX - blanco.width - blanco.width / 2) &&
        e.offsetX <= (posicionX + blanco.width - blanco.width / 2) &&
        e.offsetY >= (posicionY - blanco.height - blanco.height / 2) &&
        e.offsetY <= (posicionY + blanco.height - blanco.height / 2)) {
        return true;
    }
    return false;
}
function cambiarMovimiento() {
    if (movimiento_x < 0) {
        movimiento_x -= 1;
    }
    else {
        movimiento_x += 1;
    }
    if (movimiento_y < 0) {
        movimiento_y -= 1;
    }
    else {
        movimiento_y += 1;
    }
}
document.addEventListener('click', function (e) {
    sonidoDisparo();
    cambiarMovimiento();
    disparos -= 1;
    if (acerto(e)) {
        sonidoCampana();
        puntos += 1;
    }
    else {
        puntos -= 1;
    }
    disparosHtml.innerHTML = 'Disparos: ' + disparos;
    puntajeHtml.innerHTML = 'Puntos: ' + puntos;
    if (disparos == 0) {
        alert('Fin del Juego\n Tu puntuacion fue de: ' + puntos);
        window.location.reload();
    }
});
document.addEventListener('mousemove', function (e) {
    mouseX = e.offsetX - mira.width / 2;
    mouseY = e.offsetY - mira.width / 2;
});
function actualizar() {
    posicionX = posicionX + movimiento_x;
    posicionY = posicionY + movimiento_y;
    if (posicionX >= ancho_juego - blanco.width / 2) {
        movimiento_x = -movimiento_x;
    }
    if (posicionX <= 0 + blanco.width / 2) {
        movimiento_x = Math.abs(movimiento_x);
    }
    if (posicionY >= alto_juego - blanco.height / 2) {
        movimiento_y = -movimiento_y;
    }
    if (posicionY <= 0 + blanco.height / 2) {
        movimiento_y = Math.abs(movimiento_y);
    }
}
function borrar() {
    //borro la pantalla
    ctx.clearRect(0, 0, ancho_juego, alto_juego);
}
function pintar() {
    ctx.drawImage(blanco, posicionX - blanco.width / 2, posicionY - blanco.height / 2);
    ctx.drawImage(mira, mouseX, mouseY);
}
blanco.onload = function () {
    setInterval(function () {
        borrar();
        actualizar();
        pintar();
    }, 50);
};
